import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import './style.css'

import { pages } from '../../__constants'
import { ContacterUnExpert } from '../../__presentationals';

class DynamicBody extends Component {

  render() {
    return (
      <div className="DynamicBody">
        <Switch>
          {pages && pages.map(page => (
            <Route path={page.link} key={page.link} component={page.Component} />
          ))}
          <Route path="/contactez-nous/expert" component={ContacterUnExpert} />
        </Switch>
      </div>
    )
  }
}

export { DynamicBody };