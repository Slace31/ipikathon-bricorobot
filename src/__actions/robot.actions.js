import { robotConstants } from '../__constants'

function toggleRobotSpeaking() {
  return {
    type: robotConstants.TOGGLE_ROBOT_SPEAKING
  }
}

export const robotActions = {
  toggleRobotSpeaking
}