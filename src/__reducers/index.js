import { combineReducers } from "redux"
import { robotReducer } from './robot.reducer'

export const rootReducer = combineReducers({
  robot: robotReducer
})