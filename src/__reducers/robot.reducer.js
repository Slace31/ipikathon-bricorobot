import { robotConstants } from '../__constants'

const initialState = {
  speaking: false
}

const robotReducer = (state = initialState, action) => {
  switch (action.type) {
    case robotConstants.TOGGLE_ROBOT_SPEAKING:
      return {
        ...state,
        speaking: !state.speaking
      }
    default:
      return state
  }
}

export {
  robotReducer
}