import { createStore, applyMiddleware } from "redux"
import { rootReducer } from "../__reducers"
import { composeWithDevTools } from "redux-devtools-extension"

const initialState = {}

export const store = createStore(
  rootReducer, // create redux store using root reducer
  initialState, // initial state of the application
  composeWithDevTools(
    applyMiddleware(
      // router and other middlewares
    )
  )
)