import React, { Component } from 'react'
import { BrowserRouter } from 'react-router-dom'

import { DynamicBody } from './__containers'
import { Header, Navigation } from './__presentationals'
import './App.css'

class App extends Component {
  render() {

    return (
      <BrowserRouter>
        <div className="App">
          <Header />
          <div className="pageContent">
            <Navigation />
            <DynamicBody />
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
