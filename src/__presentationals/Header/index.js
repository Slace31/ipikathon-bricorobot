import React from 'react'
import { connect } from 'react-redux'
import './style.css'

import { RobotFace } from '..'
import { robotActions } from '../../__actions'

import { Button } from 'react-materialize'

const CHeader = ({ toggleSpeaking, robot }) => (
  <div className="Header">
    <div className="RobotMessage">
      <div className="RobotMessage-text">
        Salut !
      <br />
        Je m'appelle BricoBot et je suis votre assistant dédié. Que puis-je faire pour vous ?
      <br />
      <br />
        <Button onClick={toggleSpeaking}>{!robot.speaking ? 'Me faire parler ...' : 'OK ! Je me tais :\'('}</Button>
      </div>
    </div>
    <RobotFace />
  </div>
)

const mapStateToProps = (state) => {
  const { robot } = state
  return {
    robot
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    toggleSpeaking: () => dispatch(robotActions.toggleRobotSpeaking())
  }
}

export const Header = connect(mapStateToProps, mapDispatchToProps)(CHeader);