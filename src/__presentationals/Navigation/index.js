import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import './style.css'

import { pages } from '../../__constants'

class Navigation extends Component {
  render() {

    return (
      <div className={`Navigation`}>
        {pages && pages.map(navItem => 
          <NavLink className="NavItem"
            to={navItem.link}
            key={navItem.link}
            style={{backgroundImage: `url(${navItem.background})`}}
            activeClassName="active">
            <span className="label">{navItem.label}</span>
          </NavLink>
        )}
      </div>
    )
  }
}

export { Navigation };