import React, { Component } from 'react'
import { connect } from 'react-redux'
import './style.css'

import RobotNormal from '../../__ressources/images/robot_normal.png'
import RobotBlink from '../../__ressources/images/robot_blink.png'
import RobotMouthClosed from '../../__ressources/images/robot_mouth_closed.png'
import RobotMouthMiddle from '../../__ressources/images/robot_mouth_middle.png'

const sprites = [
  RobotNormal,
  RobotBlink,
  RobotMouthClosed,
  RobotMouthMiddle
]

class CRobotFace extends Component {
  state = {
    eyesBlinking: false
  }

  componentDidMount() {
    // window.setTimeout(this.blinkEyes, 5000)
    this.interval = window.setInterval(this.blinkEyes, 10 * 1000)
    this.loadSprites()
  }

  componentWillUnmount() {
    window.clearInterval(this.blinkEyes)
  }

  loadSprites = () => {
    for (let index = 0; index < sprites.length; index++) {
      let image = new Image()
      image.src = sprites[index]
    }
  }

  blinkEyes = () => {
    this.setState({
      eyesBlinking: true
    })
    window.setTimeout(() => this.setState({
      eyesBlinking: false
    }), 200)
  }

  render() {
    const { eyesBlinking } = this.state
    const { speaking } = this.props

    return (
      <div className={`RobotFace${eyesBlinking && !speaking ? ' blink' : ''}${speaking ? ' speaking' : ''}`}>
        {/* {sprites && sprites.map((sprite, index) => <img src={sprite} alt={`robot ${index}`} key={index} hidden/>)} */}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { robot } = state
  return {
    ...robot
  }
}

export const RobotFace = connect(mapStateToProps)(CRobotFace)