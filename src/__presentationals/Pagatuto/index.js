import React from 'react'
import './style.css'
import imgTuto from '../../__ressources/images/imgtuto.jpg'
import Print from '../../__ressources/images/print.jpg'
// import Ideedeco from '../../__ressources/images/ideedeco.jpg'
// import Montermeuble from '../../__ressources/images/montermeuble.jpg'
// import tutonul from '../../__ressources/images/tutonuls.jpg'

const Pagatuto = () => (
  <div >
<h1 className="h1">Votre tutorial : étagère murale </h1>
<img  className='print responsive-img' src={Print} alt="imprimer votre tuto" width="50px" height="50px" />

<img src={imgTuto} alt="suivez votre tuto"/>
<div class="flow-text">
  <h2>Instructions</h2>
  <p class="flow-text">Lorem ipsum dolor sit amet, 
    consectetur adipiscing elit. 
    Sed molestie dolor vel dolor volutpat efficitur. 
    Etiam vitae risus id lectus laoreet ultricies eu
     nec tortor. In pretium dignissim consectetur. 
     Maecenas aliquam leo eu mauris posuere, quis 
     varius dolor gravida. Duis quis elit ut nibh blandit 
     fringilla eget vitae sem. Quisque venenatis sodales urna 
     id mollis. Aliquam quis suscipit dui, at dapibus erat. 
     Pellentesque mattis sem cursus dictum rhoncus. 
     Cras porttitor maximus dui faucibus vehicula. 
     Nunc sodales orci a lobortis viverra. Duis posuere 
     mi ex, a hendrerit ante egestas nec</p>
  <ol>
    <h3>materiel</h3>
    <li>Deux planches de bois</li>
    <li>clous</li>
    <li>marteau</li>
    <li>scie sauteuse</li>
  </ol>

<div className="catalogue">
<h4>Exporter sa video tuto</h4>

</div>
</div>

  </div>

)

export { Pagatuto }