import React, { Component } from 'react'
import { Preloader } from 'react-materialize'
import { Player, ControlBar } from 'video-react';
import Video from '../../__ressources/videos/video.mp4'
import Expert from '../../__ressources/images/expertdyi.jpg'
import './style.css'

class ContacterUnExpert extends Component {
  state = {
    loading: true
  }

  componentDidMount() {
    window.setTimeout(() => this.setState({ loading: false }), 2000)
  }

  render() {
    const { loading } = this.state

    if (loading) {
      return (
        <div className="ContacterUnExpert">
          <Preloader size='big' />
          <p>Tentative de contact avec un expert, veuillez patienter ...</p>
        </div>
      )
    }

    return (
      <div className="ContacterUnExpert">
        <Player
          playsInline
          poster={Expert}
          src={Video}
          autoPlay={true}
        >
          <ControlBar disableCompletely={true} />
        </Player>
      </div >
    )
  }
}

export {
  ContacterUnExpert
}