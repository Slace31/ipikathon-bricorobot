import React, { Component } from "react"
import { Col, CardTitle, Card } from "react-materialize"
import TestImage from '../../__ressources/images/vis-terrasse-inox-a2-auto-perforante-P-1070736-2861338_1.jpg'

class UnNom extends Component {
  onClick = () => {
    alert("bonjour")
  }
  render() {
    return (<div onClick={this.onClick}>Bsoar</div>)
  }
}

class Catalogue extends Component {
  render() {
    return (
      <Col m={7} s={5}>
        <Card header={<CardTitle image={TestImage} ></CardTitle>} actions={[<a href=''>Buy</a>]}>
          <p>Vis/Ecrou à l'unité</p>
          Prix :
          <br></br>
          Nombre :
          <input type="number" min="0" step="1"></input>
        </Card>
        <Card horizontal header={<CardTitle image=""></CardTitle>} actions={[<a href=''>Buy</a>]}>
          <p>Outils de bricolage à l'unité</p>
          Prix :
          <br></br>
          Nombre :
          <input type="number" min="0" step="1"></input>
        </Card>
        <Card horizontal header={<CardTitle image=""></CardTitle>} actions={[<a href=''>Buy</a>]}>
          <p>Outils de jardin à l'unité</p>
          Prix :
          <br></br>
          Nombre :
          <input type="number" min="0" step="1"></input>
        </Card>
      </Col>
    )
  }
}

export { UnNom, Catalogue }