import React from 'react'
import { Link } from 'react-router-dom'
import './style.css'

export const ContactezNous = () => (
  <div className="ContactezNous">
    <Link className="expert" to="/contactez-nous/expert"><span>Parler avec un expert</span></Link>
    <Link className="vendeur" to="/contactez-nous/vendeur"><span>Demander l'aide d'un vendeur</span></Link>
  </div>
)