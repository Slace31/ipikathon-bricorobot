import 'normalize.css'
import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import registerServiceWorker from './registerServiceWorker'
import { Provider } from "react-redux";
import { store } from './__helpers'
import "../node_modules/video-react/dist/video-react.css";

ReactDOM.render(
  <Provider store={store}>
      <App />
  </Provider>,
  document.getElementById('root')
)
registerServiceWorker()
