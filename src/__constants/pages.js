import NavItemImage1 from '../__ressources/images/catalogue.jpg'
import NavItemImage2 from '../__ressources/images/montermeuble.jpg'
import NavItemImage3 from '../__ressources/images/contact.jpg'
import { ContactezNous, Pagatuto, Catalogue } from '../__presentationals';

export const pages = [
  {
    label: "Catalogue",
    link: "/catalogue",
    background: NavItemImage1,
    Component: Catalogue
  },
  {
    label: "Tuto",
    link: "/tuto",
    background: NavItemImage2,
    Component: Pagatuto
  },
  {
    label: "Contactez nous",
    link: "/contactez-vous",
    background: NavItemImage3,
    Component: ContactezNous
  }
]